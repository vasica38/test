import socket
import requests
from requests import Response

#here I should use the connection to db to check about the status of the request


def main(port : int) -> None:

    SERVER_HOST = '0.0.0.0'
    SERVER_PORT = int(port)

    # Initialize socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((SERVER_HOST, SERVER_PORT))

    server_socket.listen(1)

    print('Cache proxy is listening on port %s ...' % SERVER_PORT)

    while True:
        # Wait for client connection
        client_connection, client_address = server_socket.accept()

        # Get the client request
        request = client_connection.recv(4096).decode()
        print(request)

        # Parse HTTP headers
        headers = request.split('\n')

        top_header = headers[0].split()
        method = top_header[0]
        location = top_header[1]


        # Get the file
        content = fetch(location, True)

        ##harcoded to return a successful response at least
        #I could use some library for easier manipulation
        if content:
            response = r'''HTTP/1.0 200 OK
                            Content-Type: text/plain
                            
                            Hello, world!
                            
                            '''
        else:
            response = 'HTTP/1.0 404 NOT FOUND\n\n File Not Found'

        # Send the response and close the connection
        client_connection.sendall(response.encode())
        client_connection.close()


def fetch(filename : str, is_https)-> bool:
    if is_https:
        return requests.get("https://"+filename) == Response.ok
    else:
        return requests.get("http://"+filename) == Response.ok

    pass


if __name__ == '__main__':
    main(4097)